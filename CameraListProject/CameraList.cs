﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//using ControllerProject;
using KSLR_R_FaceRecognitionsSystem;


namespace CameraListProject
{
    public partial class CameraList : Form
    {
        //IndexController indexController = new IndexController();


        public CameraList()
        {
            InitializeComponent();
            //indexController.cameralistcontroller.VideoCapture(this.pictureBox1);
        }

        private void btnLogout_click_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas cerrar sesión?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                this.Close();
        }

        private void lblAsistencia_Click(object sender, EventArgs e)
        {
            var m = new FaceRecognitionSystem();
            m.Show();
            this.Hide();
        }
    }
}
