﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProject
{

    public abstract class ConnectionToSql
    {
        private readonly string connectionString;
        public ConnectionToSql()
        {
            // tux connectionString = "Server=LAPTOP-U6PM2KL8;Database=finalproject;Integrated Security=true;";
            // chalius:
            connectionString = "Server=DESKTOP-GMU23Q8;Database=finalproject;Integrated Security=true;";

        }

    protected SqlConnection GetConnection()
    {
        return new SqlConnection(connectionString);
    }
    }

}
