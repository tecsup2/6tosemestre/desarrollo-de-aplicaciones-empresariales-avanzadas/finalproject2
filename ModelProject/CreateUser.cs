﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelProject
{
    public class CreateUser: ConnectionToSql
    {

        public bool GetCreateUser(String Loginname, String Password, String Firstname, String Lastname, String Email, String Position)
        {
            var con = GetConnection();
            con.Open();
            String sql = "insert into Users (LoginName,Password,FirstName,LastName,Position,Email) values (@LoginName,@Password,@FirstName,@LastName,@Position,@Email)";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@LoginName", Loginname);
            cmd.Parameters.AddWithValue("@Password", Password);
            cmd.Parameters.AddWithValue("@FirstName", Firstname);
            cmd.Parameters.AddWithValue("@LastName", Lastname);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Position", Position);

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {

                return true;
            }
            else
                return false;


        }
    }




}

