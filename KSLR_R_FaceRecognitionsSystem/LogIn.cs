﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ControllerProject;

namespace KSLR_R_FaceRecognitionsSystem
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
        }

        private void btn_logIn_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text != "")
            {
                if (txtPassword.Text != "")
                {
                    UserModel user = new UserModel();
                    var validLogin = user.LoginUser(txtUsuario.Text, txtPassword.Text);
                    if (validLogin == true)
                    {
                        var m = new CamerasList();
                        m.Show();
                        this.Hide();
                    }
                    else
                    {
                        msgError("Username o contraseña incorrecta. \n  Por favor intente denuevo");
                        txtPassword.Clear();
                        txtUsuario.Focus();
                    }
                }
                else
                {
                    msgError("Por favor ingrese una contraseña");
                }
            }
            else
            {
                msgError("Por favor ingrese un username");
            }


            //this.Close();
        }

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            FaceRecognitionSystem m = new FaceRecognitionSystem();
            m.Show();
            this.Hide();
        }
        */
        private void msgError(string msg)
        {
            lblErrorMessage.Text = "     " + msg;
            lblErrorMessage.Visible = true;
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var m = new Register();
            m.Show();
            this.Hide();
        }
    }
}
