﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ControllerProject;

namespace KSLR_R_FaceRecognitionsSystem
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            ControllerCreateUser user = new ControllerCreateUser();
            user.GetCreateUser(txtUser.Text, txtPassword.Text, txtname.Text, txtApellido.Text, txtEmail.Text, "adea");

            var m = new CamerasList();
            m.Show();
            this.Hide();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            LogIn login = new LogIn();
            login.Show();
            this.Close();
        }
    }
}
