﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KSLR_R_FaceRecognitionsSystem
{
    public partial class FaceRecognitionSystem : Form
    {
        //Variables 
        MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_TRIPLEX, 0.6d, 0.6d);

        //HaarCascade Library
        HaarCascade faceDetected;

        //For Camera as WebCams 
        Capture camera;

        //Images List if Stored
        Image<Bgr, Byte> Frame;

        Image<Gray, byte> result;
        Image<Gray, byte> TrainedFace = null;
        Image<Gray, byte> grayFace = null;

        //List 
        List<Image<Gray, byte>> trainingImages = new List<Image<Gray, byte>>();

        List<string> labels = new List<string>();
        List<string> users = new List<string>();

        int Count, NumLables, t;
        string name, names = null;

        // conexión:
        #region Conexión
        SqlConnection con;
        DataSet ds = new DataSet();
        DataSet ds_asistencias = new DataSet();
        DataTable tablePerson = new DataTable();
        DataTable tablePerson_asistencias = new DataTable();
        BindingManagerBase managerBase;
        BindingManagerBase managerBase_asistencias;
        SqlCommandBuilder builder;
        SqlCommandBuilder builder_asistencias;
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlDataAdapter adapter_asistencias = new SqlDataAdapter();
        #endregion




        public FaceRecognitionSystem()
        {
            InitializeComponent();
            faceDetected = new HaarCascade("haarcascade_frontalface_alt.xml");

            try
            {
                string Labelsinf = File.ReadAllText(Application.StartupPath + "/Faces/Faces.txt");
                string[] Labels = Labelsinf.Split(',');

                NumLables = Convert.ToInt16(Labels[0]);
                Count = NumLables;

                string FacesLoad;

                for (int i = 1; i < NumLables + 1; i++)
                {
                    FacesLoad = "face" + i + ".bmp";
                    trainingImages.Add(new Image<Gray, byte>(Application.StartupPath + "/Faces/" + FacesLoad));
                    labels.Add(Labels[i]);
                }
            }
            catch (Exception)
            {
                //MessageBox.Show("Database Folder is empty..!, please Register Face");
                MessageBox.Show("El folder de imagenes está vacío. Por favor registra rostros.");
            }



            camera = new Capture();
            camera.QueryFrame();

            Application.Idle += new EventHandler(FrameProcedure);


            btnSave.Enabled = true;
            btnOpen.Enabled = true;
            btnRestart.Enabled = true;
            txName.Focus();




        }

        private void Open_Click(object sender, EventArgs e)
        {
            Process.Start(new ProcessStartInfo()
            {
                FileName = Application.StartupPath + "/Faces/",
                UseShellExecute = true,
                Verb = "open"
            });

        }

        private void Restart_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void empleadosBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.empleadosBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.finalprojectDataSet);

        }

        private void FaceRecognitionSystem_Load(object sender, EventArgs e)
        {

            EstablecerConexion();

        }

        private void ExitPro_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            managerBase.AddNew();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            managerBase.EndCurrentEdit();
            builder = new SqlCommandBuilder(adapter);
            adapter.Update(tablePerson);
            MessageBox.Show("Se modificó el registro correctamente");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            managerBase.RemoveAt(managerBase.Position);
            builder = new SqlCommandBuilder(adapter);
            adapter.Update(tablePerson);
            MessageBox.Show("Curso eliminada correcmanete");
        }

        private void btnNuevaAsistencia_Click(object sender, EventArgs e)
        {


            managerBase_asistencias.AddNew();


            if (lblName.Text == "")
            {
                txtHiddenName.Text = "";
                txtHiddenApellido.Text = "";
            }
            else if (lblName.Text == "Desconocido")
            {
                txtHiddenName.Text = lblName.Text;
            }
            else
            {
                try
                {
                    txtHiddenName.Text = lblName.Text.Substring(0, lblName.Text.IndexOf(" ", 0));
                    txtHiddenApellido.Text = lblName.Text.Substring(lblName.Text.IndexOf(" ", 0), lblName.Text.Length - 4);
                }
                catch (Exception ex)
                {
                }

            }





        }

        private void btnGuardarAsistencia_Click(object sender, EventArgs e)
        {
            txtDate.Text = DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss");
            try
            {
                if (txtHiddenApellido.Text == "")
                {

                }
                else
                {
                managerBase_asistencias.EndCurrentEdit();
                builder_asistencias = new SqlCommandBuilder(adapter_asistencias);
                adapter_asistencias.Update(tablePerson_asistencias);
                MessageBox.Show("Asistencia agregada correctamente");
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("Error al insertar el registro.");
            }

        }

        private void btnActualizarAsistencia_Click(object sender, EventArgs e)
        {
            managerBase_asistencias.EndCurrentEdit();
            builder_asistencias = new SqlCommandBuilder(adapter_asistencias);
            adapter_asistencias.Update(tablePerson_asistencias);
            MessageBox.Show("Se modificó el registro correctamente");
        }

        private void btnEliminarAsistencia_Click(object sender, EventArgs e)
        {
            managerBase_asistencias.RemoveAt(managerBase_asistencias.Position);
            builder_asistencias = new SqlCommandBuilder(adapter_asistencias);
            adapter_asistencias.Update(tablePerson_asistencias);
            MessageBox.Show("Asistencia eliminada correcmanete");
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            CamerasList view = new CamerasList();
            view.Show();
            this.Close();

        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (txName.Text == "" || txName.Text.Length < 2 || txName.Text == string.Empty)
            {
                MessageBox.Show("Please enter name of person");
            }
            else
            {
                Count += 1;
                grayFace = camera.QueryGrayFrame().Resize(320, 240, INTER.CV_INTER_CUBIC);
                MCvAvgComp[][] DetectedFace = grayFace.DetectHaarCascade(faceDetected, 1.2, 10,
                    HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20));

                foreach (MCvAvgComp f in DetectedFace[0])
                {

                    TrainedFace = Frame.Copy(f.rect).Convert<Gray, Byte>();
                    break;
                }

                TrainedFace = result.Resize(100, 100, INTER.CV_INTER_CUBIC);

                trainingImages.Add(TrainedFace);
                IBOutput.Image = TrainedFace;

                //labels.Add(txName.Text);
                labels.Add(txName.Text +" "+ textLastname.Text);

                File.WriteAllText(Application.StartupPath + "/Faces/Faces.txt", trainingImages.ToArray().Length.ToString() + ",");

                for (int i = 1; i < trainingImages.ToArray().Length + 1; i++)
                {
                    trainingImages.ToArray()[i - 1].Save(Application.StartupPath + "/Faces/face" + i + ".bmp");
                    File.AppendAllText(Application.StartupPath + "/Faces/Faces.txt", labels.ToArray()[i - 1] + ",");
                }

                /*
                MessageBox.Show("Face Stored.");
                txName.Text = "";
                txName.Focus();
                */
            try
            {
                managerBase.EndCurrentEdit();
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tablePerson);
                MessageBox.Show("Empleado agregado correctamente");
            }
            catch (Exception error)
            {
                MessageBox.Show("Error al insertar el registro.");
            }

            }


        }

        private void FrameProcedure(object sender, EventArgs e)
        {
            users.Add("");
            lblCountAllFaces.Text = "0";

            Frame = camera.QueryFrame().Resize(320, 240, INTER.CV_INTER_CUBIC);
            grayFace = Frame.Convert<Gray, Byte>();

            MCvAvgComp[][] faceDetectedShow = grayFace.DetectHaarCascade(faceDetected, 1.2, 10,
                HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20));

            foreach (MCvAvgComp f in faceDetectedShow[0])
            {
                t += 1;

                result = Frame.Copy(f.rect).Convert<Gray, Byte>().Resize(100, 100, INTER.CV_INTER_CUBIC);
                Frame.Draw(f.rect, new Bgr(Color.Green), 3);

                if (trainingImages.ToArray().Length != 0)
                {
                    MCvTermCriteria termCriterias = new MCvTermCriteria(Count, 0.001);
                    EigenObjectRecognizer recognizer =
                        new EigenObjectRecognizer(trainingImages.ToArray(),
                        labels.ToArray(), 3000,
                        ref termCriterias);

                    name = recognizer.Recognize(result);
                    Frame.Draw(name, ref font, new Point(f.rect.X - 2, f.rect.Y - 2), new Bgr(Color.Red));

                }
                users[t - 1] = name;
                users.Add("");
                //Set the number of faces detected on the scene
                lblCountAllFaces.Text = faceDetectedShow[0].Length.ToString();
                users.Add("");

            }

            t = 0;

            //Names concatenation of persons recognized
            for (int nnn = 0; nnn < faceDetectedShow[0].Length; nnn++)
            {
                names = names + users[nnn] + ", ";
            }

            //Show the faces procesed and recognized
            cameraBox.Image = Frame;
            lblName.Text = names;

            //txtHiddenName.Text = txName.Text;
            //txtHiddenApellido.Text = textLastname.Text;


            /*
            if(lblName.Text == "" )
            {
            txtHiddenName.Text = "";
            txtHiddenApellido.Text = "";
            }
            else if (lblName.Text == "Desconocido")
            {
                txtHiddenName.Text = lblName.Text;
            }
            else
            {
                try
                {
                    txtHiddenName.Text = lblName.Text.Substring(0, lblName.Text.IndexOf(" ", 0));
                    txtHiddenApellido.Text = lblName.Text.Substring(lblName.Text.IndexOf(" ", 0), lblName.Text.Length-4);
                }catch(Exception ex){
                }

            }
            */

            names = "";
            users.Clear();

        }


        private void EstablecerConexion()
        {
            String str = "Server=DESKTOP-GMU23Q8;DataBase=finalproject;Integrated Security=true;";
            con = new SqlConnection(str);



            String sql = "SELECT * FROM Empleados";
            SqlCommand cmd = new SqlCommand(sql, con);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds, "Empleados");
            tablePerson = ds.Tables["Empleados"];
            dgvEmpleados.DataSource = tablePerson;

            /*txtPersonID.DataBindings.Add("text", tablePerson, "PersonID");
            txtFirstName.DataBindings.Add("text", tablePerson, "FirstName");
            txtLastName.DataBindings.Add("text", tablePerson, "LastName");
            txtHireDate.DataBindings.Add("text", tablePerson, "HireDate");
            txtEnrollmentDate.DataBindings.Add("text", tablePerson, "EnrollmentDate");
            */
            txName.DataBindings.Add("text", tablePerson, "FirstName");
            textLastname.DataBindings.Add("text", tablePerson, "LastName");
            
            managerBase = this.BindingContext[tablePerson];







            String sql2 = "SELECT * FROM Asistencias";
            SqlCommand cmd2 = new SqlCommand(sql2, con);
            adapter_asistencias.SelectCommand = cmd2;
            adapter_asistencias.Fill(ds_asistencias, "Asistencias");
            tablePerson_asistencias = ds_asistencias.Tables["Asistencias"];
            dgvAsistencias.DataSource = tablePerson_asistencias;

            /*txtPersonID.DataBindings.Add("text", tablePerson, "PersonID");
            txtFirstName.DataBindings.Add("text", tablePerson, "FirstName");
            txtLastName.DataBindings.Add("text", tablePerson, "LastName");
            txtHireDate.DataBindings.Add("text", tablePerson, "HireDate");
            txtEnrollmentDate.DataBindings.Add("text", tablePerson, "EnrollmentDate");
            */
            txtHiddenName.DataBindings.Add("text", tablePerson_asistencias, "Nombre");
            txtHiddenApellido.DataBindings.Add("text", tablePerson_asistencias, "Apellido");
            txtDate.DataBindings.Add("text", tablePerson_asistencias, "Date");
            
            managerBase_asistencias = this.BindingContext[tablePerson_asistencias];


        }





    }

}
