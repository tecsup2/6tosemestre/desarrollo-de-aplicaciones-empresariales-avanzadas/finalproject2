﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelProject;

namespace ControllerProject
{
    public class UserModel
    {
        UserDAO userdao = new UserDAO();
        public bool LoginUser(string user, string pass)
        {
            return userdao.Login(user, pass);
        }
    }
}
