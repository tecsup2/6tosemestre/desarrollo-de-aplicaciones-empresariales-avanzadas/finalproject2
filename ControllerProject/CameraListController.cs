﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace ControllerProject
{
    public class CameraListController
    {

        #region VideoCapture
        private Capture videoCapture = null;
        private static Image<Bgr, Byte> currentFrame = null;
        Mat frame = new Mat();
        //CascadeClassifier faceCascadeClassifier = new CascadeClassifier("haarcascade_frontalface_alt2.xml");
        CascadeClassifier faceCascadeClassifier = new CascadeClassifier("haarcascade_fullbody.xml");
        //CascadeClassifier faceCascadeClassifier = new CascadeClassifier("haarcascade_upperbody.xml");
        PictureBox pictureBox1;
        #endregion

        #region FaceDetection
        private bool facesDetectionEnabled = false;
        #endregion

        Rectangle face;

        public void VideoCapture(PictureBox pictureBox1)
        {

            this.pictureBox1 = pictureBox1;

            // INICIAR CAPTURA:
            videoCapture = new Capture();
            videoCapture.ImageGrabbed += ProcessFrame;
            videoCapture.Start();

            // INICIAR RECONOCIMIENTO FACIAL:
            facesDetectionEnabled = true;

        }



        private void ProcessFrame(object sender, EventArgs e)
        {
            // Step 1: Video capture
            videoCapture.Retrieve(frame, 0);
            currentFrame = frame.ToImage<Bgr, Byte>().Resize(pictureBox1.Width, pictureBox1.Height, Inter.Cubic);

            DetectFaces();

            // Render the video capture into the Picture box picCapture
            pictureBox1.Image = currentFrame.Bitmap;


        }

        private void DetectFaces()
        {
            // Step 2: Face detection
            if (facesDetectionEnabled)
            {
                // Convert from Bgr to Gray Image
                Mat grayImage = new Mat();
                CvInvoke.CvtColor(currentFrame, grayImage, ColorConversion.Bgr2Gray);
                // Enhance the image to get better result
                CvInvoke.EqualizeHist(grayImage, grayImage);

                Rectangle[] faces = faceCascadeClassifier.DetectMultiScale(grayImage, 1.1, 3, Size.Empty, Size.Empty);
                // If faces detected
                if (faces.Length > 0)
                {
                    foreach (var face1 in faces)
                    {
                        face = face1;
                        // Draw square around each face
                        CvInvoke.Rectangle(currentFrame, face, new Bgr(Color.Red).MCvScalar, 2);



                    }
                }
            }
        }


    }
}
